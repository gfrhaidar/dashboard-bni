package com.project.bni.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {

    @GetMapping("/")
    public String dashboard(){

        return "dashboard";
    }

    @GetMapping("/login")
    public void login(){

    }

    @GetMapping("/card")
    public void layout(){

    }
}
